<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        $data = [
            'header' => 'Halo!',
            'nav' => 'home'
        ];
        return view('/index', $data);
    }
    public function about(){
        $data = [
            'header' => 'More about me',
            'nav' => 'about'
        ];
        return view('/about', $data);
    }
    public function contact(){
        $data = [
            'header' => 'Call Me',
            'nav' => 'contact'
        ];
        return view('/contact', $data);
    }
    public function portfolio(){
        $projects = [
            [
                'name' => 'Portfolio',
                'project' => 'UI',
                'technology' => 'Figma',
                'img' => 'portfolio.png'
            ],
            [
                'name' => 'Planty',
                'project' => 'Web App',
                'technology' => 'PHP, MySLl, Js',
                'img' => 'planty.png'
            ],
            [
                'name' => 'SSO HMJ TI',
                'project' => 'Web App',
                'technology' => 'PHP, CI3, MySQL, Js',
                'img' => 'sso.png'
            ],
            [
                'name' => 'Kirim Tugas',
                'project' => 'Web App',
                'technology' => 'React Js, Express, MongoDB',
                'img' => 'kirimtugas.png'
            ],
            [
                'name' => 'Wigneswara',
                'project' => 'Web App',
                'technology' => 'PHP, CI3, MySQL, Js',
                'img' => 'wigneswara.png'
            ],
            [
                'name' => 'Wiwahane',
                'project' => 'UI',
                'technology' => 'Figma',
                'img' => 'wiwahane.png'
            ],
            [
                'name' => 'Smart Kolok',
                'project' => 'UI',
                'technology' => 'Figma',
                'img' => 'kolok.png'
            ],
            [
                'name' => 'Garnity',
                'project' => 'IOT, Mobile App',
                'technology' => 'NodeMCU, Arduino, Firebase, React Native, Express',
                'img' => 'garnity.png'
            ],
            [
                'name' => 'Nabung',
                'project' => 'Mobile App',
                'technology' => 'MongoDB, Firebase, React Native, Express',
                'img' => 'nabung.png'
            ],
        ];
    
        $data = [
            'header' => 'Show Case',
            'nav' => 'portfolio',
            'projects' => $projects
        ];
        return view('/portfolio', $data);
    }
    public function blog($id = null){
        $blogs = [
            [
                'id' => '1',
                'title' => 'Pelatihan Manajemen Konten',
                'date' => '13 Juli 2021',
                'tag' => 'kkn, kuliah',
                'img' => 'manajemen.jpg',
                'content' => 'Pelatihan pertama yang telah terlaksana pada hari Senin, 12 Juli 2021 adalah pelatihan manajemen konten. Kegiatan pelatihan dilaksanakan secara daring melalui platform Zoom dan berlangsung  selama 3 jam yakni dari pukul 17.00 WITA hingga 20.00 WITA. Manajemen konten itu sendiri merupakan hal yang pertama perlu diketahui oleh pembisnis yang dalam konteks ini adalah masyarakat sasararan. Manajemen konten dalam media sosial dikenal sebagai media sosial manajemen yang berarti sebuah proses menciptakan menerbitkan mempromosikan serta mengelola konten di semua saluran media sosial seperti Instagram, Facebook, Twitter linkedin, YouTube, Pinterest, dan sebagainya. Media sosial manajemen lebih dari sekedar membagikan dan memposting di media sosial. Akan tetapi sosial media managemen juga berperan dalam tingkat interaksi bisnis atau masyarakat sasararan terhadap pembeli. Selain itu, dengan media sosial manajemen membantu masyarakat sasaran untuk menjangkau lebih banyak pembeli serta visibilitas bisnis.
                Sebagai sebuah teknik atau keterampilan, media sosial manajemen perlu diasah dan diterapkan atau dicoba secara langsung sehingga masyarakat sasaran mengetahui letak kesulitan dalam menerapkan sosial media manajemen secara langsung. Berkaitan dengan hal tersebut, pelatihan yang telah berlangsung memiliki dua tahapan yakni tahap pemberian materi dan tahap penerapan. Selama tahap penerapan atau latihan implementasi , banyak masyarakat yang bertanya mengenai pemilihan jadwal yang tepat untuk pembeli. Selain itu juga, terdapat juga yang bertanya mengenai aplikasi yang dapat menunjang pembuatan jadwal kegiatan yang berisi fitur pengingat.'
            ],
            [
                'id' => '2',
                'title' => 'Pendampingan Digital Marketing Melalui Media Sosial pada Remaja Perintis Usaha di Desa Selat, Kecamatan Sukasada secara Door to Door',
                'date' => '27 Juli 2021',
                'tag' => 'kkn, kuliah',
                'img' => 'pelatihan.jpeg',
                'content' => 'Dalam upaya meningkatkan pemahaman masyarakat sasaran terhadap materi yang telah diberikan dan dijelaskan yakni Digital Marketing dan juga dalam rangka melakukan pendekatan serta melibatkan langsung masyarkat sasaran dengan proyek KKNT yang telah saya rancang, saya melakukan pertemuan secara langsung (luring) terhadap masyrakat sasaran. Pertemuan dilakukan secara Door to Door atau dengan kata lain saya mengunjungi masyarakat perorang dengan tujuan masyarakat sasaran dapat lebih terbuka untuk berdiskusi. Di lain sisi, hal ini juga bertujuan untuk mengurangi resiko penularan COVID-19 yang kian meningkat. Pertemuan dilakukan dengan memperhatikan protokol kesehatan yang ketat. Setiap pertemuan berlangsung kurang lebih selama 2 jam yang dilaksanakan pada tanggal 25-26 Juli 2021. Pertemuan ini menitikberatkan pada kendala yang dialami masyarakat sasaran dalam penerapan Digitial Marketing pada usaha yang mereka rintis.
    
                Semua pertemuan berlangsung dengan lancar. Setiap masyarakat sasaran memiliki antusias yang tinggi untuk berdiskusi. Tidak hanya mendiskusikan kendala yang mereka hadapi, masyarakat sasaran juga banyak yang bertanya mengenai tips meningkatkan penjualan mereka secara digital seperti tips untuk melakukan editing foto di smartphone, membuat flyer produk, dan lain sebagainya. Sebagai contoh salah satu masyarakat sasaran atas nama Gede Adi Purnawan yang merupakan perintis usaha warung kopi menanyakan bagaimana cara membuat menu makanan dan minuman dengan menarik.
                
                Kegiatan ini merupakan kegiatan akhir atau penutup program KKNT yang saya rancang dan jalankan. Adapun kendala ringan yang saya hadapi tidak hanya pada pertemuan ini, tetapi juga pada kegiatan lain sebelumnya adalah susahnya menemukan waktu yang pas untuk melakukan kegiatan. Kendala ini terbilang wajar karena mengingat masyarakat sasaran memiliki kesibukan yakni sedang menjalankan usaha mereka. Meskipun demikian, melihat antusias mereka, besar harapan saya bahwa proyek KKNT ini dapat meyakinkan mereka untuk menjalankan Digital Marketing pada usahanya sehingga statistik penjualannya dapat meningkat.'
            ]
        ];

        $id_pos = array_search($id, array_column($blogs, 'id'));
        
        if($id_pos !== false){
            $data = [
                'header' => $blogs[$id_pos]['title'],
                'nav' => 'blog-post',
                'blog' => $blogs[$id_pos],
            ];
            return view('/blog-post', $data);
        }else{
            $data = [
                'header' => 'My Activity',
                'nav' => 'blog',
                'blogs' => $blogs
            ];
            return view('/blog', $data);
        }
    }
}
