<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8">
    <title>{{ $header }} - Jepri Kusuma</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- icon -->
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/icon.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/icon.png')}}">

    <!-- Template Google Fonts -->
    <link href="{{asset('fonts.googleapis.com/cssdda2.css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i')}}" rel="stylesheet">
    <link href="{{asset('fonts.googleapis.com/cssab6d.css?family=Open+Sans:300,400,400i,600,600i,700')}}" rel="stylesheet">

    <!-- Template CSS Files -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/preloader.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/circle.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/fm.revealator.jquery.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <!-- CSS Skin File -->
    <link href="{{asset('css/skins/yellow.css')}}" rel="stylesheet">

    <!-- Modernizr JS File -->
    <script src="js/modernizr.custom.js"></script>
</head>